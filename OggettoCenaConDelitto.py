#contiene le classi per lo script di cena con delitto

import random, time

import costanti_cena_con_delitto as costanti

from OggettoIndizio import oggetto_indizio
from OggettoSospettato import oggetto_sospettato

class oggetto_cena_con_delitto:
	#oggetto cutilizzato per passare tutti i parametri da una funzione alla successiva
	lista_persone = costanti.par_lista_persone
	lista_caratteristiche = costanti.par_lista_caratteristiche
	flag_dichiarazioni = costanti.par_flag_dichiarazioni
	tipologia_indizi = costanti.par_tipologia_indizi
	flag_elimina_indizi_superflui = costanti.par_flag_elimina_indizi_superflui
	min_caratteristiche_per_persona = costanti.par_min_caratteristiche_per_persona
	max_caratteristiche_per_persona = costanti.par_max_caratteristiche_per_persona
	min_caratteristiche_per_indizio =  costanti.par_min_caratteristiche_per_indizio
	max_caratteristiche_per_indizio = costanti.par_max_caratteristiche_per_indizio
	flag_assassino_parla_di_assassino = costanti.par_flag_assassino_parla_di_assassino 
	flag_conoscenza_numero_caratteristiche_persona = costanti.par_flag_conoscenza_numero_caratteristiche_persona
	flag_tutte_caratteristiche_prima_fine = costanti.par_flag_tutte_caratteristiche_prima_fine
	flag_autoreferenziazione = costanti.par_flag_autoreferenziazione
	flag_solo_assassino_tutte_caratteristiche = costanti.par_flag_solo_assassino_tutte_caratteristiche

	#parametri che mi serviranno in seguito
	assassino = None
	dizionario_caratteristiche = None

	#costruttore vuoto
	def __init__(self):
		pass

	#costruttore con parametri
	# def __init__(self, p_lista_persone, p_lista_caratteristiche, p_flag_dichiarazioni, p_tipologia_indizi, p_flag_elimina_indizi_superflui, p_caratteristiche_per_persona, p_min_caratteristiche_per_indizio, p_max_caratteristiche_per_indizio, p_flag_assassino_parla_di_assassino, p_assassino = None):
	# 	#costruttore con assegnamento dei parametri
	# 	self.lista_persone = p_lista_persone 
	# 	self.lista_caratteristiche = p_lista_caratteristiche 
	# 	self.flag_dichiarazioni = p_flag_dichiarazioni 
	# 	self.tipologia_indizi = p_tipologia_indizi 
	# 	self.flag_elimina_indizi_superflui = p_flag_elimina_indizi_superflui
	# 	self.caratteristiche_per_persona = p_caratteristiche_per_persona
	# 	self.min_caratteristiche_per_indizio = p_min_caratteristiche_per_indizio 
	# 	self.max_caratteristiche_per_indizio = p_max_caratteristiche_per_indizio
	# 	self.flag_assassino_parla_di_assassino = p_flag_assassino_parla_di_assassino
	# 	self.assassino = p_assassino

	def stringa_soluzione(self):
		#metodo per generare la stringa della soluzione con le caratteristiche per ciascun giocatore
		stringa = 'Lista caratteristiche personaggi\n'
		for persona in self.lista_persone:
			stringa += persona + ': ' + ', '.join(self.dizionario_caratteristiche[persona]) + '\n'
		return stringa

	def assassino_casuale(self):
		#metodo per scegliere casualmente un assassino
		self.assassino = random.choice(self.lista_persone)

	def assegna_caratteristiche(self):
		#funzione per dividere le caratteristiche tra le persone
		if self.assassino == None:
			self.assassino_casuale()
		#adesso l'assassino ha un nome
		#assegno le varie caratteristiche
		self.dizionario_caratteristiche = {}
		for persona in self.lista_persone:
			#controllo se il flag che l'assassino e l'unico con tutte le caratteristiche e' segnato
			if self.flag_solo_assassino_tutte_caratteristiche == True:
				#distinguo se assassino o no
				if persona == self.assassino:
					numero_caratteristiche = len(self.lista_caratteristiche)
				else:
					#non e l'assassino prendo un numero di caratteristiche -1 rispetto al massimo
					numero_caratteristiche = random.randrange(
						self.min_caratteristiche_per_persona, min(self.max_caratteristiche_per_persona+1, len(self.lista_caratteristiche) ))
			else:
				#calcolo la lista delle caratteristiche
				numero_caratteristiche = random.randrange(self.min_caratteristiche_per_persona, self.max_caratteristiche_per_persona+1)
			ins_caratterisctiche_persona = set(random.sample(self.lista_caratteristiche, numero_caratteristiche))
			#controllo che non sia gia presente
			n_iter = 0
			while (ins_caratterisctiche_persona in self.dizionario_caratteristiche.values()) and n_iter < costanti.numero_max_iter:
				#controllo se il flag che l'assassino e l'unico con tutte le caratteristiche e' segnato
				if self.flag_solo_assassino_tutte_caratteristiche == True:
					#distinguo se assassino o no
					if persona == self.assassino:
						numero_caratteristiche = len(self.lista_caratteristiche)
					else:
						#non e l'assassino prendo un numero di caratteristiche -1 rispetto al massimo
						numero_caratteristiche = random.randrange(
							self.min_caratteristiche_per_persona, min(self.max_caratteristiche_per_persona+1, len(self.lista_caratteristiche)))
				else:
					#calcolo la lista delle caratteristiche
					numero_caratteristiche = random.randrange(
						self.min_caratteristiche_per_persona, self.max_caratteristiche_per_persona+1)
					ins_caratterisctiche_persona = set(random.sample(self.lista_caratteristiche, numero_caratteristiche))
					n_iter +=1
			#se esco e il numero di iterazioni ha superato il numero massimo di iterazioni vado in errore
			if n_iter >= costanti.numero_max_iter:
				self.assegna_caratteristiche()
				return None
			#al termine aggiungo ad dizionario
			self.dizionario_caratteristiche[persona] = ins_caratterisctiche_persona.copy()
		#al termine aggiungo asnche l'assassino
		self.dizionario_caratteristiche[costanti.label_assassino] = self.dizionario_caratteristiche[self.assassino]
		#l'assassino deve avere almeno una caratteristica diversa d tutti se gli indizi sono solo affermativi
		if self.tipologia_indizi == costanti.tipologia_indizio_affermativo:
			for persona in self.lista_persone:
				if persona != self.assassino:
					#controllo che abbia almeno una caratteristica diversa
					if self.dizionario_caratteristiche[self.assassino].issubset(self.dizionario_caratteristiche[persona]):
						#non hanno elementi diversi, devo richiamare la funzione a ricorsione
						self.assegna_caratteristiche()
						break
		#al termine l'assassino dovrebbe essere sempre individuabile

	def genera_indizio(self):
		#metodo per generare un indizio
		#controllo la modalita
		if self.tipologia_indizi == costanti.tipologia_indizio_affermativo:
			lista_tipologie = [costanti.tipologia_indizio_affermativo]
		elif self.tipologia_indizi == costanti.tipologia_indizio_negativo:
			lista_tipologie = [costanti.tipologia_indizio_negativo]
		elif self.tipologia_indizi == costanti.tipologia_indizio_entrambi:
			lista_tipologie = [costanti.tipologia_indizio_affermativo, costanti.tipologia_indizio_negativo]
		#ne scelgo una a caso
		tipologia = random.choice(lista_tipologie)
		#scelgo la persona di riferimento
		persona_scelta = random.choice(list(self.dizionario_caratteristiche.keys()))
		#valuto le caratteristiche tra cui scegliere
		if tipologia == costanti.tipologia_indizio_affermativo:
			#prendo solo le caratteristiche associate
			lista_caratteristiche_da_scegliere = list(self.dizionario_caratteristiche[persona_scelta])
		elif tipologia == costanti.tipologia_indizio_negativo:
			lista_caratteristiche_da_scegliere = [i for i in self.lista_caratteristiche if i not in self.dizionario_caratteristiche[persona_scelta]]
		#se lista caratteristiche e vuoto richiamo la stessa funzione
		if len(lista_caratteristiche_da_scegliere) == 0:
			return self.genera_indizio()
		#ora devo prendere il sottoinsieme della lista delle caratteristiche
		numero_caratteristiche_da_scegliere = random.randrange( min(len(lista_caratteristiche_da_scegliere), self.min_caratteristiche_per_indizio), min(len(lista_caratteristiche_da_scegliere), self.max_caratteristiche_per_indizio) +1 )
		lista_caratteristiche_scelte = random.sample( lista_caratteristiche_da_scegliere,  numero_caratteristiche_da_scegliere)
		#adesso devo scegliere eventualmente in dichiarante
		if self.flag_dichiarazioni:
			#devo scegliere il dichiarante
			lista_dichiaranti = self.lista_persone[:]
			#verifico se togliere il riferimento all'indizio
			if (self.flag_autoreferenziazione == False) and (persona_scelta in lista_dichiaranti):
				lista_dichiaranti.remove(persona_scelta)
			#verifico se si parla dell'assassino e l'assassino puo autoreferenziarsi
			if (self.flag_assassino_parla_di_assassino == True) and (persona_scelta == costanti.label_assassino) and (self.assassino in lista_dichiaranti):
				lista_dichiaranti.remove(self.assassino)
			#ora calcolo il dichiarante
			dichiarante = random.choice(lista_dichiaranti)
		else:
			#il dichiarante e none
			dichiarante = None
		#ora posso generare l'indizio
		indizio_generato = oggetto_indizio(p_tipologia_indizio=tipologia, p_caratteristiche_coinvolte=lista_caratteristiche_scelte, p_persona_riferimento=persona_scelta, p_dichiarante=dichiarante)
		#ritorno l'indizio generato
		return indizio_generato

	def conta_possibili_soluzioni(self, lista_indizi):
		#funzione per contare quante possibili soluzioni ci sono con la lista di indizi fornita
		#mi creo il dizionario dei sospettati
		diz_sospettati = dict()
		for persona in self.dizionario_caratteristiche.keys():
			diz_sospettati[persona] = oggetto_sospettato(p_caratteristiche=self.lista_caratteristiche)
		#adesso devo scorrere la lista degli indizi e ripetere il tutto fino a quando non ho piu modifiche
		#flag_modifica = True:
		#while flag_modifica = True:
		#	flag_modifica = False
			#scorro tutti gli indizi credo basti un giro soltanto
		for indizio in lista_indizi:
			#mi calcolo l'ggetto sospettato
			oggetto_sospettato_riferimento = diz_sospettati[indizio.persona_riferimento]
			#applico l'indizio
			indizio.applica_indizio_sospettato(sospettato=oggetto_sospettato_riferimento)
			#se conosco il numero esatto di caratteristiche provo a vedere se quelle caratteristiche bastano
			if self.flag_conoscenza_numero_caratteristiche_persona and (indizio.persona_riferimento != costanti.label_assassino):
				oggetto_sospettato_riferimento.verifica_numero_caratteristiche_conosciute(numero=len(self.dizionario_caratteristiche[indizio.persona_riferimento]))
		#adesso devo verificare se ho l'obbligo di conoscere tutte le caratteristiche delle altre persone prima di potermi pronunciare sull'assassino
		if self.flag_tutte_caratteristiche_prima_fine:
			#devo scorrere tutti i sospettati e vedere se le loro caratteristiche sicure sono esattamente quelle assegnate
			for persona in diz_sospettati.keys():
				oggetto_sospettato_persona = diz_sospettati[persona]
				if self.dizionario_caratteristiche[persona] != oggetto_sospettato_persona.ins_caratteristiche_sicure:
					#non va bene ritorno il 2
					return 2
		#adesso ho terminato di scorrere tutti gli indizi. verifico se posso con certezza sapere chi sia l'assassino
		lista_possibili_assassini = self.lista_persone[:]
		oggetto_sospettato_assassino = diz_sospettati[costanti.label_assassino]
		#adesso vedo per ciascuno se posso eliminarlo dalla lista di possibili assassini
		for persona in self.lista_persone:
			#la persona non puo essere l'assassino se uno dei due ha una caratteristica sicura che l'altro sicuramente non ha
			oggetto_sospettato_persona = diz_sospettati[persona]
			if (len(oggetto_sospettato_assassino.ins_caratteristiche_sicure - oggetto_sospettato_persona.tutte_caratteristiche_possibili() )>0 ) or (len(oggetto_sospettato_persona.ins_caratteristiche_sicure - oggetto_sospettato_assassino.tutte_caratteristiche_possibili() )>0 ):
				lista_possibili_assassini.remove(persona)
		#terminato il ciclo guardo quanti sono i possibili assassini
		if len(lista_possibili_assassini) == 0:
			#errore
			raise Exception('Nessun assassino trovato')
		elif len(lista_possibili_assassini) == 1:
			return 1
		else:
			#piu assassini
			return 2
			
	def genera_lista_indizi(self):
		#metodo per generare la lista degli indizi
		lista_indizi = []
		#adesso ciclo fino a quando non trovo una soluzione unica
		while self.conta_possibili_soluzioni(lista_indizi=lista_indizi) != 1:
			#genero un nuovo indizio
			nuovo_indizio = self.genera_indizio()
			#verifico che un indizio uguale non sia gia presente
			while nuovo_indizio in lista_indizi:
				nuovo_indizio = self.genera_indizio()
			#aggiungo l'indizio
			lista_indizi.append(nuovo_indizio)
		#a questo punto verifico se devo eliminare gli indizi superflui
		if self.flag_elimina_indizi_superflui:
			#devo scorrere tutti gli indizi
			indice = 0
			while indice < len(lista_indizi):
				#calcolo l'indizio da togliere
				indizio_da_togliere = lista_indizi[indice]
				#lo tolgo
				lista_indizi.remove(indizio_da_togliere)
				#verifico cosa fare 
				if self.conta_possibili_soluzioni(lista_indizi=lista_indizi) != 1:
					#lo devo riaggiungere
					lista_indizi.insert(indice, indizio_da_togliere)
					indice +=1
				else:
					#indizio superfluo
					print('Indizio eliminato')
		#al termine posso ritornare la lista degli indizi
		return lista_indizi

	def genera_stringa_parametri(self):
		#metodo per generare la stringa dei parametri utilizzati
		stringa = '''PARAMETRI:
Lista Personaggi : {lista_persone}
Lista Caratteristiche : {lista_caratteristiche}
Indizi Dichiarazioni : {flag_dichiarazioni}
Tipologia Indizi : {tipologia_indizi}
Elimina Indizi Superflui : {flag_elimina_indizi_superflui}
Numero minimo caratteristiche per persona : {min_caratteristiche_per_persona}
Numero massimo caratteristiche per persona : {max_caratteristiche_per_persona}
Numero minimo caratteristiche per indizio :  {min_caratteristiche_per_indizio}
Numero massimo caratteristiche per indizio : {max_caratteristiche_per_indizio}
Assassino può dare indizi su Assassino : {flag_assassino_parla_di_assassino} 
Conoscenza numero caraatteristiche per persona : {flag_conoscenza_numero_caratteristiche_persona}
Conoscere tutte le caratteristiche di tutti prima di accusare : {flag_tutte_caratteristiche_prima_fine}
Autoreferenziazione negli indizi : {flag_autoreferenziazione}
Solo assassino con tutte le caratteristiche: {flag_assassino_tutte_caratteristiche}
Assassino: {assassino}
'''.format(lista_persone=self.lista_persone,
			lista_caratteristiche=self.lista_caratteristiche,
			flag_dichiarazioni=self.flag_dichiarazioni,
			tipologia_indizi=self.tipologia_indizi,
			flag_elimina_indizi_superflui=self.flag_elimina_indizi_superflui,
			min_caratteristiche_per_persona=self.min_caratteristiche_per_persona,
			max_caratteristiche_per_persona=self.max_caratteristiche_per_persona,
			min_caratteristiche_per_indizio=self.min_caratteristiche_per_indizio,
			max_caratteristiche_per_indizio=self.max_caratteristiche_per_indizio,
			flag_assassino_parla_di_assassino=self.flag_assassino_parla_di_assassino,
			flag_conoscenza_numero_caratteristiche_persona=self.flag_conoscenza_numero_caratteristiche_persona,
			flag_tutte_caratteristiche_prima_fine=self.flag_tutte_caratteristiche_prima_fine,
			flag_autoreferenziazione=self.flag_autoreferenziazione,
			flag_assassino_tutte_caratteristiche=self.flag_solo_assassino_tutte_caratteristiche,
			assassino=self.assassino)
		return stringa



	def genera_file_output(self, lista_indizi):
		#metodo per generare il file di output con gli indizi e la soluzione
		#apro il file
		data_ora = time.strftime('%Y%m%d_%H%M%S')
		f = open(costanti.nome_file_output%(data_ora), 'w' )
		#scrivo la lista degli indizi
		f.write('CENA CON DELITTO PARTITA N° ' + data_ora + '\n\n')
		f.write(self.genera_stringa_parametri() + '\n\n')
		f.write('INDIZI\n')
		#scrivo tutti gli indizi
		#mescolo gli indizi
		copia_lista_indizi = lista_indizi[:]
		random.shuffle(copia_lista_indizi)
		for indizio in copia_lista_indizi:
			f.write(indizio.to_string() + '\n')
		#salto una rica
		f.write('\n')
		#scrivo la soluzione
		f.write('SOLUZIONE\n')
		f.write(self.stringa_soluzione())
		f.write('\n')
		f.write(costanti.label_assassino + ' è ' + self.assassino)
		f.close()

	def gestisci_creazione_gioco(self):
		#metodo per gestire la creazione del gioco
		#per prima cosa assegno le caratteristiche
		self.assegna_caratteristiche()
		#poi genera la lista degli indizi
		lista_indizi = self.genera_lista_indizi()
		#infine scrive nel file
		self.genera_file_output(lista_indizi=lista_indizi)
		
