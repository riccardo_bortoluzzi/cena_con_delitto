#file di costanti della cena con delitto

import os

#paramtri default
par_lista_persone = ['Giacomo', 'Luigi', 'Carlo', 'Matteo', 'Fabio', 'Sara', 'Michela', 'Andrea']
par_lista_caratteristiche = ['altezza elevata', 'capelli biondi', 'gambe storte', 'neo in fronte', 'passione per la lettura', 'occhiali', 'vestiti stravaganti', 'complesso di inferiorità', 'casa al mare', 'un figlio']
par_flag_dichiarazioni = False #flag per dire che gli indizi sono sotto forma di dichiarazioni
par_tipologia_indizi = 2 #0 significa solo negativi, 1 solo affermativi e 2 sia affermativi che negativi
par_flag_elimina_indizi_superflui = False #se a fine generazione ripassa tutti gli indizi per eliminare quelli superflui
par_min_caratteristiche_per_persona = 3 #numero minimo di caratteristiche per ogni persona
par_max_caratteristiche_per_persona = 5 #numero massimo di caratteristiche per persona
par_min_caratteristiche_per_indizio = 1 #numero minimo di caratteristiche che compaiono in ciascun indizio
par_max_caratteristiche_per_indizio = 3 #numero massimo di caratteristiche per ogni indizio
par_flag_assassino_parla_di_assassino = False #flag che indica se l'assassino possa o meno fare dichiarazioni riguardanti se stesso
par_flag_conoscenza_numero_caratteristiche_persona = False #flag che indica se i giocatori conoscono il numero di caratteristiche di ciascun giocatore
par_flag_tutte_caratteristiche_prima_fine = False #flag che indica se prima di poter accusare una persona devo poter essere in grado di stabilire tutte le caratteristiche degli altri partecipanti
par_flag_autoreferenziazione = False #se una persona puo fare un'affermazione su se stesso
par_flag_solo_assassino_tutte_caratteristiche = False #se solo l'assassino avra tutte le caratteristiche

#costanti in generale
verbo_utilizzato = 'ha'
numero_max_iter = 100

#cartella_programma = os.path.abspath(os.path.dirname(__file__))
cartella_programma = os.path.abspath('.')
nome_file_output = os.path.join(cartella_programma, 'cena_con_delitto_%s.txt')
tipologia_indizio_affermativo = 1
tipologia_indizio_negativo = 0
tipologia_indizio_entrambi = 2
label_assassino = "L'assassino"

#per log
file_log = os.path.join(cartella_programma, 'log_cena_con_delitto.log')
dim_max_log = 1024*1024*5
