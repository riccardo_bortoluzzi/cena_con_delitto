# -*- coding: utf-8 -*-

#script con le funzioni generali per la cena con delitto

import costanti_cena_con_delitto as costanti

import sys
import traceback
import time
import os


def scrivi_log(funzionalita_errore=''):
	#funzione per scrivere il file di log
	#print('Scrittura log')
	etype, value, tb = sys.exc_info()
	stringa_da_scrivere = time.strftime('%d/%m/%Y %H:%M:%S') + ' - ' + 'funzionalità in errore: ' + \
            funzionalita_errore + '\n' + \
            ''.join(traceback.format_exception(etype, value, tb)) + '\n\n'
	#se il file di log non e presente lo creo
	if not os.path.exists(costanti.file_log):
		f = open(costanti.file_log, 'w')
		f.close()
	#scrive un log
	logfile = open(costanti.file_log, 'r')
	contenuto_log = logfile.read()
	logfile.close()
	contenuto_log += stringa_da_scrivere
	#lo tronco
	contenuto_log = contenuto_log[-costanti.dim_max_log:]
	logfile = open(costanti.file_log, 'w')
	#logfile.write(stringa_da_scrivere)
	logfile.write(contenuto_log)
	logfile.close()

