Programma per generare gli indizi di una cena con delitto.
Il colpevole è stato chiamato Assassino.
Gli indizi possono riguardare uno dei partecipantio oppure riferirsi all'Assassino.

Per generare gli indizi della Cena con Delitto avviare il programma 
CenaConDelitto.exe
(L'antivirus potrebbe segnalarlo come virus, 
semplicemente perchè è un software che non conosce e nuovo, ma garantisco che non è affatto pericoloso)

Una volta avviato il programma si dovrebbe avviare la grafica dove inserire i parametri.
Se non chiara, rendere la finestra a schermo intero.

Inserire i nomi dei personaggi nell'apposita linea di inserimento 
sotto il rettangolo bianco in alto a sinistra premendo "Aggiungi" o tramite il tasto "Invio".
Il nome, se non già presente nella lista, apparirà nel rettangolo bianco soprastante.
Per eliminare un nome selezionarlo e premere il tasto "Rimuovi".
Si possono anche ordinare i nomi delle persone. 
Il primo nome in alto rappresenta l'assassino.

Si può procedere allo stesso modo per inserire tutte le caratteristiche presenti nella storia
utilizzando la sezione a destra dell'inserimento dei personaggi.
Le funzionalità sono e stesse.
Per impostazione predefinita, negli indizi generati le caratteristiche saranno accompagnate 
dal verbo "ha" (es: le caratteristiche "un cavallo" e "18 anni" si legherebbero con l'indizio 
"Andrea ha un cavallo, 18 anni").
Le caratteristiche di ogni personaggio (compreso l'assassino) verranno scelte casuali
(per il momento, se servisse posso provare ad implementare la scelta delle caratteristiche
per ogni personaggio oppure eventualmente solo per l'assassino) in modo comunque che 
l'assassino sia unicamente identificabile tra i sospettati.

I parametri relativi al numero di caratteristiche per ogni persona rappresentano
un intervallo tra cui si sceglierà quante caratteristiche associare ad ogni persona.
Si intendono i numeri di minimo e massimo compresi nell'intervallo.
Per ottenere un numero prestabilito di caratteristiche per ogni persona inserire come 
minimo e come massimo lo stesso valore.
Nel caso in cui l'intervallo comprenda più di un valore, ogni personaggio potrebbe avere
un numero di caratteristiche differente gli uni dagli altri. 
Il numero nell'intervallo è scelto casualmente.

Analogo discorso per quanto riguarda il numero di caratteristiche presenti in ogni indizio.
Questo rappresenta gli estremi di un intervallo tra cui scegliere il numero di caratteristiche 
di ogni indizio. 
Tale intervallo sarà rispettato solo nel caso in cui ci siano abbastanza caratteristiche.
(es: Andrea ha 2 caratteristiche eppure come numero minimo di caratteristiche per indizio è inserito 4. 
Gli indizi affermativi che riguarderanno Andrea avranno il numero massimo di 
caratteristiche disponibili, quindi 2).

Si può scegliere l'opzione che permette di suddividere 
le caratteristiche per ogni personaggio
in modo che solo l'assassino le possieda tutte
e le altre persone abbiano un numero di caratteristiche consono
con i numeri inseriti poco sopra
impedendo che uno di loro possieda 
anch'esso tutte le caratteristiche dell'assassino.

Gli indizi si dividono in 2 categorie:
1) indizi affermativi in cui si associa una caratteristica ad una persona 
	(es: Andrea ha un cavallo)
2) indizi negativi in cui si afferma che una persona 
	non possiede una (o più) determinata(/e) caratteristica(/he)
	(es: Andrea non ha 18 anni, un cane)
Attraverso gli appositi pulsanti si possono scegliere di utilizzare una, l'altra tipologia o entrambe.
NB: Nel caso si scelgano solo indizi affermativi o solo indizi negativi è necessario, 
affinché il gioco termini, che gli utenti conoscano il numero di 
caratteristiche di ogni partecipante (vedi seguito).

Gli utenti possono conoscere o meno il numero di caratteristiche per ogni partecipante
(es: tutti gli utenti sanno che il personaggio Andrea possiede 3 carattersitiche).
Tale dato può essere utile nel momento in cui un personaggio
può possedere al massimo il numero di caratteristiche conosciuto
oppure quando si è sicuri che un utente possieda sicuramente un determinato numero di caratteristiche.

Gli indizi generati possono essere associati o meno alle dichiarazioni delle persone.
Tale opzione non è obbligatoria e l'associazione dichiarante-indizio avviene in maniera casuale
rispettando eventualmente alcune piccole regole (vedi poi).

Nel caso in cui si scelga di far affermare gli indizi alle persone,
c'è la possibilità di evitare che una persona pronunci un indizio riguardante sè stessa
(es: Andrea afferma: Andrea ha gli occhi azzuri).

Si può anche scegliere se l'assassino possa o meno pronunciare degli indizi
che aiuterebbero nella ricerca dello stesso 
(es: se Andrea fosse l'assassino, 
se possa o meno pronunciare una frase del tipo "L'Assassino ha un cavallo" ).

Una volta terminata la generazione degli indizi,
tramite apposito pulsante si può scegliere se eliminare quelle dichiarazioni
che risultano superflue per lindividuazione del colpevole.
Scegliendo questa opzione, il numero di indizi mostrati potrebbe risultare molto piccolo.

L'ultima opzione da poter scegliere permette di stabilire se, 
prima di poter accusare una persona, sia necessario o meno saper attribuire
ad ogni personaggio le rispettive caratteristiche.

Una volta scelti i parametri premere il pulsante in fondo "Genera Indizi".

Verrà mostrato un messaggio di inizio calcolo e la finestra si disattiverà
(non sarà possibile premere pulsanti o cambiare opzioni) fino al termine della procedura.
Un messaggio avviserà del termine del calcolo, sia nel caso di terminazione corretta
che di terminazione errata. Nel caso in cui si verifichi un errore, nella stessa cartella 
in cui è presente il programma, si genererà un file
chiamato "log_cena_con_delitto.log". Inviare questo file a Riccardo
per maggiori spiegazioni.
In ogni caso la procedura non dovrebbe impiegare più di qualche secondo
per terminare. In caso di parecchi personaggi e caratteristiche
potrebbe richiedere un po' più tempo. Se la computazione dura 
più di qualche minuto chiudere il programma e riaprirlo.

Se il programma dovesse terminare correttamente,
nella stessa cartella in cui si trova il programma,
verrà creato un file di testo chiamato "cena_con_delitto_data_ora.txt"
dove data sarà del tipo GiornoMeseAnno e ora OraMinutiSecondi.
All'interno si trovano inizialmente i parametri utilizzati,
la lista degli indizi o dichiarazioni e la soluzione finale
con tutte le caratteristiche per ogni personaggio.

Buon divertimento!!!

Se ci fossero dubbi o qualcosa non funzionasse non esitare
a contattare Riccardo.

