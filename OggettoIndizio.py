#script per contenere l'oggetto indizio

import costanti_cena_con_delitto as costanti

class oggetto_indizio:
	#inizializzo gli attributi
	dichiarante = None
	tipologia_indizio = None #affermativo 1, negativo 0
	caratteristiche_coinvolte = None
	persona_riferimento = None

	def __init__(self, p_tipologia_indizio, p_caratteristiche_coinvolte, p_persona_riferimento, p_dichiarante = None):
		#costruttore del metodo
		self.tipologia_indizio = p_tipologia_indizio
		self.caratteristiche_coinvolte = set(p_caratteristiche_coinvolte)
		self.dichiarante = p_dichiarante
		self.persona_riferimento = p_persona_riferimento

	def __eq__(self, indizio2):
		#metodo per verificare se 2 indizi sono uguali
		#se hanno stesso riferimento e stessa lista di caratteristiche
		return ((self.persona_riferimento == indizio2.persona_riferimento) and self.caratteristiche_coinvolte == indizio2.caratteristiche_coinvolte)

	def to_string(self):
		#metodo per convertire l'indizio in stringa
		stringa = ''
		if self.dichiarante is not None:
			stringa += self.dichiarante + ': '
		#adesso compongo la stringa 
		stringa += self.persona_riferimento + ' '
		#adesso devo valutare la tipologia indizio
		if self.tipologia_indizio == costanti.tipologia_indizio_affermativo:
			stringa += costanti.verbo_utilizzato + ' '
		elif self.tipologia_indizio == costanti.tipologia_indizio_negativo:
			stringa += 'non ' + costanti.verbo_utilizzato + ' '
		#adesso devo mettere le caratteristiche
		lista_caratteristiche = list(self.caratteristiche_coinvolte)
		if len(lista_caratteristiche) >1:
			stringa += ', '.join(lista_caratteristiche[: -1]) + \
                    ' e ' + lista_caratteristiche[-1]
		else:
			#una sola caratteristica
			stringa += lista_caratteristiche[0]
		#chiudo la stringa
		#stringa += '\n'
		return stringa
		
	def applica_indizio_sospettato(self, sospettato):
		#metodo per applicare l'indizio al sospettato
		if self.tipologia_indizio == costanti.tipologia_indizio_negativo:
			#devo togliere la lista di caratteristiche
			return sospettato.elimina_lista_caratteristiche(lista_caratteristiche=self.caratteristiche_coinvolte)
		elif self.tipologia_indizio == costanti.tipologia_indizio_affermativo:
			return sospettato.stabilizza_lista_caratteristiche(lista_caratteristiche=self.caratteristiche_coinvolte)
		else:
			#errore
			raise Exception('Tipologia indizio non contemplata applica indizio dizionario caratteristiche')
