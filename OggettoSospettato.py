#script che contiene l'oggetto che rappresenta un sospettato con la relativa lista di caratteristiche possibili e sicure

class oggetto_sospettato:
	ins_caratteristiche_possibili = None
	ins_caratteristiche_sicure = None

	def __init__(self, p_caratteristiche):
		#funzione costruttore
		self.ins_caratteristiche_possibili = set(p_caratteristiche)
		self.ins_caratteristiche_sicure = set() #insieme vuoto inizialmente non si sa niente

	def tutte_caratteristiche_possibili(self):
		#funzione che ritorna l'insieme di tutte le caratteristiche 
		return self.ins_caratteristiche_possibili.union(self.ins_caratteristiche_sicure)

	def rendi_sicura_caratteristica(self, caratteristica):
		#metoco che sposta una caratteristica da un insieme all'altro
		if caratteristica in self.ins_caratteristiche_possibili:
			self.ins_caratteristiche_possibili.remove(caratteristica)
		if caratteristica not in self.ins_caratteristiche_sicure:
			self.ins_caratteristiche_sicure.add(caratteristica)
			return True #ho apportato una modifica
		else:
			return False #non ho apportato la modifica

	def rendi_impossibile_caratteristica(self, caratteristica):
		#funzioen per eliminare definitivamente una caratteristicha
		modifica = False
		if caratteristica in self.ins_caratteristiche_possibili:
			self.ins_caratteristiche_possibili.remove(caratteristica)
			modifica = True
		if caratteristica in self.ins_caratteristiche_sicure:
			self.ins_caratteristiche_sicure.remove(caratteristica)
			modifica = True
		#alla fine ritorno la modifica er sapere se ho fatto operazioni
		return modifica

	def elimina_lista_caratteristiche(self, lista_caratteristiche):
		#funzione per eliminare la lista delle caratteristiche
		modifica = False
		for carattersitica in lista_caratteristiche.copy():
			modificato_oggetto = self.rendi_impossibile_caratteristica(caratteristica=carattersitica)
			modifica = modifica or modificato_oggetto
		return modifica
	
	def stabilizza_lista_caratteristiche(self, lista_caratteristiche):
		#funzione per rendere sicura una lista di caratteristiche
		modifica = False
		for carattersitica in lista_caratteristiche.copy():
			modificato_oggetto = self.rendi_sicura_caratteristica(caratteristica=carattersitica)
			modifica = modifica or modificato_oggetto
		return modifica

	def verifica_numero_caratteristiche_conosciute(self, numero):
		#metodo per verificare se si conosce il numero di caratteristiche del sospettato e in caso affermativo di rendere sicure quelle che si hanno
		if numero < len(self.tutte_caratteristiche_possibili()):
			#devo verificare se quelle attualmente sicure sono in numero uguale
			if numero == len(self.ins_caratteristiche_sicure):
				#devo eliminare tutte le possibili
				return self.elimina_lista_caratteristiche(lista_caratteristiche=self.ins_caratteristiche_possibili)
			else:
				#non posso dire niente ritorno False
				return False #nessuna modifica
		elif numero == len(self.tutte_caratteristiche_possibili()):
			#devo prendere quelle possibili e renderle sicure
			return self.stabilizza_lista_caratteristiche(lista_caratteristiche=self.ins_caratteristiche_possibili)
		else:
			#numero > lunghezza -> errore
			raise Exception('Numero caratteristiche aspettate maggiore di possibili')
		#alla fine non serve piu ritornare niente

